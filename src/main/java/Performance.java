/**
 * 
 */

/**
 * @author Minglun Zhang
 *
 */

public class Performance {
	private String playID;
	private int audience;
	Play play;
	int amount;
	int volumeCredit;

	public Performance(String playID, int audience) {
		this.playID = playID;
		this.audience = audience;
	}

	public void enrichPerformance(Performance aPerformance) {
		this.play = BillPrint.playFor(aPerformance);
	}

	public String getPlayID() {
		return playID;
	}

	public void setPlayID(String playID) {
		this.playID = playID;
	}

	public int getAudience() {
		return audience;
	}

	public void setAudience(int audience) {
		this.audience = audience;
	}

	public void setPlayFor(Play play) {
		this.play = play;
	}
	public void setAmountFor(int amount) {
		this.amount = amount;
	}

	public void setVolumeCreditFor(int volumeCredit) {
		this.volumeCredit = volumeCredit;
	}
}
