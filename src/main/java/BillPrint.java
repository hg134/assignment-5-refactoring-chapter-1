import java.sql.Statement;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * BillPrint - adapted from Refactoring, 2nd Edition by Martin Fowler
 *
 * @author Minglun Zhang
 *
 */
public class BillPrint {
	private static HashMap<String, Play> plays;
	private String customer;
	private ArrayList<Performance> performances;

	public BillPrint(ArrayList<Play> plays, String customer, ArrayList<Performance> performances) {
		this.plays = new HashMap();
		for (Play p: plays) { this.plays.put(p.getId(),p); }

		this.customer = customer;
		this.performances = performances;
	}

	public HashMap<String, Play> getPlays() {
		return plays;
	}


	public String getCustomer() {
		return customer;
	}

	public ArrayList<Performance> getPerformances() {
		return performances;
	}

	// decompose amountFor and change the variable
	public int amountFor(Performance aPerformance) {
		int result = 0;
		switch (aPerformance.play.getType()) {
			case "tragedy": result = 40000;
				if (aPerformance.getAudience() > 30) {
					result += 1000 * (aPerformance.getAudience() - 30);
				}
				break;
			case "comedy":  result = 30000;
				if (aPerformance.getAudience() > 20) {
					result += 10000 + 500 * (aPerformance.getAudience() - 20);
				}
				result += 300 * aPerformance.getAudience();
				break;
			default:        throw new IllegalArgumentException("unknown type: " +  playFor(aPerformance).getType());
		}
		return result;
	}


	// decompose the play
	public static Play playFor(Performance aPerformance) {
		return plays.get(aPerformance.getPlayID());
	}

	// the function to  calculate the credit
	public int volumeCreditsFor(Performance aPerformance) {
		int result= 0;
		result += Math.max(aPerformance.getAudience() - 30, 0);
		if ( aPerformance.play.getType().equals("comedy")) {
			result += Math.floor((double) aPerformance.getAudience() / 5.0);
		}
		return result;
	}

	public String usd(double aNumber) {
		DecimalFormat numberFormat = new DecimalFormat("#.00");
		String result = "";
		result = numberFormat.format(aNumber);
		return result;
	}

	// function to calculate the total credits
	public int totalVolumeCredits(statementData data) {
		int result = 0;
		// add volume credits
		for (Performance perf: data.performance) {
			result += perf.volumeCredit;
		}
		return result;
	}

	public int totalAmount(statementData data) {
		int result = 0;
		for (Performance perf: data.performance) {
			result += perf.amount;
		}
		return result;
	}

	public String statement () {

		return renderPlainText(createStatementData());
	}

	// set a new varianle of performance and set its play using playFor()
	public void enrichPerformance(Performance aPerformance) {
		aPerformance.setPlayFor(playFor(aPerformance));
		aPerformance.setAmountFor(amountFor(aPerformance));
		aPerformance.setVolumeCreditFor(volumeCreditsFor(aPerformance));
	}

	public statementData createStatementData() {
		statementData statement = new statementData(this.customer, this.performances);
		for (Performance perf: statement.performance) {
			enrichPerformance(perf);
		}
		statement.totalAmount = totalAmount(statement);
		statement.totalCredit = totalVolumeCredits(statement);
		return statement;
	}

	public String renderPlainText(statementData data) {
		String result = "Statement for " + data.customer + "\n";

		for (Performance perf: data.performance) {
			if (perf.play == null) {
				throw new IllegalArgumentException("No play found");
			}

			int thisAmount = amountFor(perf);

			// print line for this order
			result += "  " + perf.play.getName() + ": $" + usd((double) perf.amount / 100.00) +
					" (" + perf.getAudience() + " seats)" + "\n";
		}

		result += "Amount owed is $" + usd( (double) data.totalAmount / 100) + "\n";
		result += "You earned " + data.totalCredit + " credits" + "\n";
		return result;
	}


	public static void main(String[] args) {
		Play p1 = new Play("hamlet", "Hamlet", "tragedy");
		Play p2 = new Play("as-like", "As You Like It", "comedy");
		Play p3 = new Play("othello", "Othello", "tragedy");
		ArrayList<Play> pList = new ArrayList<Play>();
		pList.add(p1);
		pList.add(p2);
		pList.add(p3);
		Performance per1 = new Performance("hamlet", 55);
		Performance per2 = new Performance("as-like", 35);
		Performance per3 = new Performance("othello", 40);
		ArrayList<Performance> perList = new ArrayList<Performance>();
		perList.add(per1);
		perList.add(per2);
		perList.add(per3);
		String customer = "BigCo";
		BillPrint app = new BillPrint(pList, customer, perList);
		System.out.println(app.statement());
	}

}
